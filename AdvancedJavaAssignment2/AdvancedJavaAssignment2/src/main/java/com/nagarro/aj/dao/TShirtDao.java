package com.nagarro.aj.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.nagarro.aj.entity.TShirt;
import com.nagarro.aj.util.HibernateUtil;

public class TShirtDao {
	public void addList(List<TShirt> list) {
		Transaction transaction = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			for(TShirt shirt : list) {
				session.save(shirt);
			}
			transaction.commit();
			session.close();
		}catch(Exception e) {
			if(transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	
	}
	public List<TShirt> getTShirt(){
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			return session.createQuery("from TShirt", TShirt.class).list();
		}
	}
}
