package com.nagarro.aj.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.nagarro.aj.entity.TShirt;
import com.nagarro.service.ProcessRequest;

public class ProcessRequestImpl implements ProcessRequest {

	@Override
	public List<TShirt> processReq(TShirt customer, List<TShirt> parser) {
		// TODO Auto-generated method stub
		List<TShirt> list = parser.stream().filter(x-> (x.getColor().equalsIgnoreCase(customer.getColor()))
    			&& (x.getGender().equalsIgnoreCase(customer.getGender())) 
    			&& (x.getSize().equalsIgnoreCase(customer.getSize())&& x.getAvailability().equalsIgnoreCase("y")))
				.collect(Collectors.toList());
		return list;
	}

}
