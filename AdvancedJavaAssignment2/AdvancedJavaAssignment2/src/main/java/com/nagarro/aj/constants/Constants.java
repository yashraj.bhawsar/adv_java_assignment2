package com.nagarro.aj.constants;

public class Constants {
	public static final String ASK_COLOR = "Enter COLOR: ";
	public static final String ASK_SIZE = "Enter SIZE(S/M/L/XL/XXL): ";
	public static final String ASK_GENDER = "Enter GENDER: ";
	public static final String ASK_OPTION_PREFERENCE = "Enter OPTION:\n1. SORT BY PRICE\n2. SORT BY RATING\n3. SORT BY BOTH ";
}

