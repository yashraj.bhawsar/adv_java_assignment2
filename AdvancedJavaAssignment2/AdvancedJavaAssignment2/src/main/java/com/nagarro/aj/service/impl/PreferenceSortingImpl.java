package com.nagarro.aj.service.impl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.nagarro.aj.entity.TShirt;
import com.nagarro.service.PreferenceSorting;

public class PreferenceSortingImpl implements PreferenceSorting {

	@Override
	public List<TShirt> preference(TShirt customer, List<TShirt> collect) {
		// TODO Auto-generated method stub
		
		if(customer.getOutputPreference()==1) {
    		collect = collect.stream().sorted(new Comparator<TShirt>() {

				@Override
				public int compare(TShirt o1, TShirt o2) {
					if(Double.parseDouble(o1.getPrice())> Double.parseDouble(o2.getPrice()))
						return 1;
					else return -1;
					
				}
    			
			}).collect(Collectors.toList());
    	}
    	else if(customer.getOutputPreference()==2) {
    		collect = collect.stream().sorted(new Comparator<TShirt>() {

				@Override
				public int compare(TShirt o1, TShirt o2) {
					if(Double.parseDouble(o1.getRating())< Double.parseDouble(o2.getRating()))
						return 1;
					else return -1;
					
				}
    			
			}).collect(Collectors.toList());
    	}
    	else if(customer.getOutputPreference()==3) {
    		collect = collect.stream().sorted(new Comparator<TShirt>() {

				@Override
				public int compare(TShirt o1, TShirt o2) {
					if(Double.parseDouble(o1.getPrice())> Double.parseDouble(o2.getPrice()))
						return 1;
					else return -1;
					
				}
    			
			}).collect(Collectors.toList());
    		collect = collect.stream().sorted(new Comparator<TShirt>() {

    			@Override
				public int compare(TShirt o1, TShirt o2) {
					if(Double.parseDouble(o1.getRating()) < Double.parseDouble(o2.getRating()))
						return 1;
					else return -1;
					
				}
    			
			}).collect(Collectors.toList());
    		
    	}
    	else {
    		collect.forEach(System.out::println);
    	}
		
		return collect;

	}

}
