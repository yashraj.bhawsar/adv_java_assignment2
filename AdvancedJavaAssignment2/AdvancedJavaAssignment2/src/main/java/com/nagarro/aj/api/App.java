package com.nagarro.aj.api;


import java.io.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;


import com.nagarro.aj.dao.TShirtDao;
import com.nagarro.aj.entity.TShirt;
import com.nagarro.aj.service.impl.CSVParserImpl;
import com.nagarro.aj.service.impl.DisplayImpl;
import com.nagarro.aj.service.impl.InputRequirementsImpl;
import com.nagarro.aj.service.impl.PreferenceSortingImpl;
import com.nagarro.aj.service.impl.ProcessRequestImpl;

public class App {
	public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String choice = null;

		
		List<TShirt> tshirts;
		CSVParserImpl csvToObjects = new CSVParserImpl();
		tshirts = csvToObjects.parser("C:\\Users\\yashrajbhawsar\\Desktop\\java\\Advance Java\\Advance Java\\Assigment Links\\Assigment Links\\Adidas.csv");
		tshirts.addAll(csvToObjects.parser("C:\\Users\\yashrajbhawsar\\Desktop\\java\\Advance Java\\Advance Java\\Assigment Links\\Assigment Links\\Nike.csv"));
		tshirts.addAll(csvToObjects.parser("C:\\Users\\yashrajbhawsar\\Desktop\\java\\Advance Java\\Advance Java\\Assigment Links\\Assigment Links\\Puma.csv"));
		TShirtDao dao = new TShirtDao();
		dao.addList(tshirts);
		
		do {
			TShirt customerTShirt = new InputRequirementsImpl().input();
			List<TShirt> reqList = new ProcessRequestImpl().processReq(customerTShirt, tshirts);
			List<TShirt> preference = new PreferenceSortingImpl().preference(customerTShirt, reqList);
			if (preference.isEmpty()) {
				System.out.println("Your choice is not available right now!");
			} else {
				new DisplayImpl().show(preference);
			}
			System.out.println("do you want to try again?");
			while (!((choice = br.readLine()).equalsIgnoreCase("y") || choice.equalsIgnoreCase("n")))
                System.out.print("Please try again:");
		}while(choice.equalsIgnoreCase("y"));
	}

}
