package com.nagarro.aj.service.impl;

import java.util.*;
import java.io.*;

import com.nagarro.aj.constants.Constants;
import com.nagarro.aj.entity.TShirt;
import com.nagarro.service.InputRequirements;

public class InputRequirementsImpl implements InputRequirements {

	@Override
	public TShirt input() {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
        System.out.println("---------------------Tshirt checker---------------------");

		System.out.println(Constants.ASK_COLOR);
		String color = sc.next();
		System.out.println(Constants.ASK_SIZE);
		String size = sc.next();
		System.out.println(Constants.ASK_GENDER);
		String gender = sc.next();
		System.out.println(Constants.ASK_OPTION_PREFERENCE);
		int opPreference = 0;
		try {
			opPreference = sc.nextInt();
		}catch(InputMismatchException msg) {
			System.out.println("Please choose valid option");
		}
			
		TShirt input = new TShirt();
		input.setColor(color);
		input.setSize(size);
		input.setGender(gender);
		input.setOutputPreference(opPreference);
		return input;
	}

}
