package com.nagarro.service;

import java.util.List;

import com.nagarro.aj.entity.TShirt;

public interface CSVParser {
	 List<TShirt> parser(String f);
}
