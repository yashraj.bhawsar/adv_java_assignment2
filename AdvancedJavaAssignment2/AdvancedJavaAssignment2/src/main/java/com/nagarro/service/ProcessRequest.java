package com.nagarro.service;

import java.util.List;

import com.nagarro.aj.entity.TShirt;

public interface ProcessRequest {
	List<TShirt> processReq(TShirt t, List<TShirt> list);
}
