package com.nagarro.service;

import com.nagarro.aj.entity.TShirt;

public interface InputRequirements {
	TShirt input();
}
