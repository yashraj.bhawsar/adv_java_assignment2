package com.nagarro.service;

import java.util.List;

import com.nagarro.aj.entity.TShirt;

public interface PreferenceSorting {
	List<TShirt> preference(TShirt customer, List<TShirt> list);
}
